
How to run orthofinder with docker:

1. Download fasta files from Uniprot proteomes into /path/to/fasta_directory:
https://www.uniprot.org/proteomes/

2. pull orthofinder docker by following instructions from:
https://hub.docker.com/r/cmonjeau/orthofinder/

############ need to be done only once
#Usage
##Pull from dockerhub
docker pull cmonjeau/orthofinder

##Or build from this repository
git clone https://github.com/cmonjeau/docker-orthofinder
cd docker-orthofinder
docker build -t orhtofinder .

############

3. Run a container
docker run -it --rm -v "/path/to/fasta_directory":/input cmonjeau/orthofinder orthofinder.py -f /input -t n_blast_processes -a n_orthofinder_threads -S diamond
docker run -it --rm -v "/path/to/results_directory":/input cmonjeau/orthofinder trees_for_orthogroups.py /input/ -t 7


def find_ortho(entry:str):
    if 'pd' not in globals():
        global pd
        import pandas as pd
        print('pd')
    if 'new_df' not in globals():
        global new_df
        new_df=pd.read_parquet('/app/data/ortho_amit.parquet')
        print('new_df')        
    return new_df[new_df['entries_in_group'].apply(lambda x: entry in x)].to_dict(orient='records')
