DATA_DIR='/data/'
APP_DIR='/app/'

import pandas as pd, numpy as np, matplotlib.pyplot as plt, os, sys, joblib
from tqdm import tqdm_notebook as tqdm

sys.path.append('/app/')

#DATA_DIR = '/mnt/wrkdir/gene_discovery2/data/'
mem = joblib.Memory(location = 'cache', verbose=0)
NUMERIC_MERGED_FILE_NAME = f'{DATA_DIR}gene_data_merged_all_NUMERIC.parquet'