import os
import pandas as pd
import requests
from tqdm import tqdm as tqdm
import numpy as np
from io import StringIO
import multiprocessing
from config import *
from gene_discovery.globals import *
import datetime
import time
import dask.dataframe as dd
import urllib.parse
import urllib.request 

####uniprot

def get_mapping(organism,cache=False):
    ''' get uniprot to ensemble mapping per organisms, to merge expression and uniprot data'''
    
    df = pd.read_parquet(DATA_DIR+organism+'_uniprot.parquet')
    file_address=os.path.join(DATA_DIR,organism+'_mapping.parquet') 
    if not cache or not os.path.isfile(file_address):    
        url = "https://www.uniprot.org/uploadlists/"   
        if df.shape[0]>2000:
            batchs=np.arange(0,df.shape[0],2000)
        else:
            batchs=[0,df.shape[0]]
        
        uni_map = pd.DataFrame()
        for i in tqdm(range(0,len(batchs)-1)):
            num=0+i
            ids = ' '.join((df['Entry'].values.tolist())[batchs[num]:batchs[num+1]])

            params = {
            'from': 'ACC+ID',
            'to': 'ENSEMBLGENOME_ID',
            'format': 'tab',
            'query': ids
            }

            data = urllib.parse.urlencode(params)
            data = data.encode('utf-8')
            req = urllib.request.Request(url, data)
            with urllib.request.urlopen(req) as f:
                response = f.read()
            #print(response.decode('utf-8'))
            tofrom = pd.read_csv(StringIO(response.decode('utf-8')),sep='\t')
            tofrom.drop_duplicates(subset=['From'],inplace=True)
            uni_map = pd.concat([uni_map,tofrom])
            i+=1
        uni_map.to_parquet(file_address)

        
def merge_mapping_all(organisms:list=list(crops_dict.keys()),cache=True): 
    '''downloads and merges all mapping files for all organisms to one mapping file'''    
    for org in tqdm(organisms):
        get_mapping(org,cache)
    df_all = pd.DataFrame()
    MAP_FILES = [f for f in os.listdir(DATA_DIR) if f.endswith('mapping.parquet')]
    df_all = pd.concat([pd.read_parquet(DATA_DIR+file) for file in MAP_FILES])
    df_all.to_csv(DATA_DIR+'uni_to_ensemble.csv')

                  
def uniprot_download_organism(organism,cache=True):  
    tax_id=crops_dict[organism]['tax_id']
    url  = 'https://www.uniprot.org/uniprot/?query=organism:%s&columns=%s&format=tab'%(tax_id,columns)
    file_address=os.path.join(DATA_DIR,organism+'_uniprot.parquet') 
    if not cache or not os.path.isfile(file_address):
        req = requests.get(url)    
        if req.status_code==200 and req.text!="":
            
            df=pd.read_table(StringIO(req.text),sep='\t',error_bad_lines=False)
            df.to_parquet(file_address)
            print('downloaded: '+organism+' to location '+file_address)
        else:
            print('download issue: '+organism)
    else:
        print(organism+' located in: '+file_address)


def uniprot_download_organisms(organisms:list=list(crops_dict.keys()),cache=True):  

    def worker(organism,cache):
        #print(crp)
        uniprot_download_organism(organism,cache)
        #print ('Worker')


    jobs = []
    for org in tqdm(organisms):
        p = multiprocessing.Process(target=worker, args=(org,cache))
        #p = multiprocessing.Process(target=worker)
        jobs.append(p)
        p.start()
    #         jobs[-1].terminate()

    for j in jobs:
        j.join()

        
###ncbi

def ncbigetsummary(db,query,webenv,st):
    url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db='+db+'&retstart='+str(st)+'&retmax=500&retmode=json&query_key='+query+'&WebEnv='+webenv
    req=requests.get(url)
    if req.status_code==200 and req.text!="":
        return req.json()
    print(req.status_code)


def ncbigetcrop(db,organism):
    '''
    returns a json to be used in ncbigethist and ncbiloopfetch/summary
    '''

    url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db='+db + '&term=txid' + str(organism) + '[Organism]&usehistory=y&retmax=1000000600&retmode=json'
    req=requests.get(url)
    if req.status_code==200 and req.text!="":
        return req.json()
    print(req.status_code)

def ncbigethist(query_result):
    return query_result['esearchresult']['querykey'] ,query_result['esearchresult']['webenv'] ,query_result['esearchresult']['idlist']


def get_ncbi_gene_crop(organism,db='gene',cache=True):
    '''
    download all genes for selected organism from gene db in ncbi
    '''
    tax_id=crops_dict[organism]['tax_id']    
    #print(directory)
    file_address=os.path.join(DATA_DIR,organism+'_ncbi.parquet')   
    if not cache or not os.path.isfile(file_address):    
        print('downloading '+organism+' from ncbi...')         
        query,webenv,id_list=ncbigethist(ncbigetcrop(db,tax_id))
        num=len(id_list)
        frames=[ncbigetsummary(db,query,webenv,x) for x in tqdm(np.arange(int(num/500)+2)*500)]
        temp=[]
        for i in range(len(frames)):
            if 'result' in list(frames[i].keys()):
                temp.append(pd.DataFrame(pd.DataFrame(frames[i])[:-3]['result'].values.tolist()))

        pd.concat(temp).astype(str).to_parquet(file_address)
        print(organism+' located in: '+file_address)         
    else:
        print(organism+' located in: '+file_address)        
    
    
def ncbi_download_organisms(organisms:list=list(crops_dict.keys()),cache=True):
    '''
    fetch a combined large file of all the data associated in uniprot with 
    specific organisms. 
    The organisms are entered as a list of taxonomy IDs
    '''

           
    for organism in organisms:
        get_ncbi_gene_crop(organism,cache=cache)
         
        
#### expression

def array_express(crop):

    #print(ogr)
    req=requests.get('https://www.ebi.ac.uk/arrayexpress/json/v3/experiments?species='+crop+'&gxa=True')
    if req.status_code==200:
        data=pd.DataFrame(req.json())
        if 'experiment' not in data.T.columns.tolist():
            print('no experiment for '+crop)
            return 'no',[]
        array_express1=pd.DataFrame(data.T.experiment.values[0]) 
        entry_list=array_express1.accession.values.tolist()
        return array_express1,entry_list
    print(req.status_code)


def crop_array_express(organism,cache=True):
    #print(directory)
    latin_name=crops_dict[organism]['latin_name'] 
    file_address=os.path.join(DATA_DIR,organism+'_exp.parquet') 
    if not cache or not os.path.isfile(file_address):       
        array_express1,entry_list=array_express(latin_name)
        p1='https://www.ebi.ac.uk/gxa/experiments-content/'
        #u2='/resources/ExperimentDownloadSupplier.Microarray/query-results'
        u2='/download/RNASEQ_MRNA_BASELINE'
        #p2='/download/RNASEQ_MRNA_DIFFERENTIAL'
        print('downloading expression...')  
        reqs=[requests.get(p1+ x+ u2).text for x in tqdm(entry_list)]
        frames=[]
        for i in tqdm(range(len(reqs))):
            gene_id=reqs[i].find('Gene ID')
            if gene_id!=-1:
                frames.append(pd.read_table(StringIO(reqs[i][gene_id:]),sep='\t',error_bad_lines=False))
        if frames==[]:
            pd.DataFrame().to_csv(file_address)
        else:
            pd.concat(frames).to_parquet(file_address)
        print(organism+' located in: '+file_address)         
    else:
        print(organism+' located in: '+file_address)        
#         else:
#             print('fail')
#     if frames!=[]:
#         return merge_all_expression_dfs_list(frames)
#     else:
#         return pd.DataFrame()


def merge_all_expression_dfs_list(df_list):
    all_genes = set([c  for d in df_list for c in d['Gene ID'].values])
    base = pd.DataFrame(list(all_genes), columns=['Gene ID'])
    oriented_df = []
    for df in tqdm(df_list):
        df = df.drop_duplicates('Gene ID')
        new_df = pd.merge(base, df, how='left', on='Gene ID', sort=False)      
        #new_df.index = new_df['Gene ID']
        to_drop = list(set(['Gene ID', 'Gene Name', 'Design Element']).intersection(set(new_df.columns)))
        new_df.drop(columns=to_drop, inplace=True)
        new_df = new_df.astype(np.float32)
        oriented_df.append(new_df)

    oriented_df = pd.concat(oriented_df, axis=1)
    oriented_df.index = base['Gene ID']
    return oriented_df


def exp_download_organisms(organisms:list=list(crops_dict.keys()),cache=True): 

    def worker(organism,cache):
        #print(crp)
        crop_array_express(organism,cache)
        #print ('Worker')


    jobs = []
    for organism in tqdm(organisms):
        p = multiprocessing.Process(target=worker, args=(organism,cache))
        #p = multiprocessing.Process(target=worker)
        jobs.append(p)
        p.start()
    #         jobs[-1].terminate()

    for j in jobs:
        j.join()

        
def exp_merge_all(datadir=DATA_DIR,cache=True,file_address=DATA_DIR+'array_express_raw_data_download.parquet'): 
    if not cache or not os.path.isfile(file_address):    
        files=list(filter(lambda x: 'exp.parquet' in x,os.listdir(datadir)))
        frames=[]
        for x in tqdm(files):
            try:
                frames.append(pd.read_parquet(datadir+x))
            except:
                pass
        df=merge_all_expression_dfs_list(frames)
        df.columns=df.columns+np.arange(len(df.columns.tolist())).astype(str)
        df.to_parquet(file_address)
        return df
    else:
        return pd.read_parquet(file_address)        


def uniprot_merge_all(datadir=DATA_DIR,file_address=DATA_DIR+'uniprot_raw_data_download.parquet',cache=True):
    if not cache or not os.path.isfile(file_address):    
        files=list(filter(lambda x: 'uniprot.parquet' in x,os.listdir(datadir)))
        frames=[pd.read_parquet(datadir+x) for x in tqdm(files)]
        df=pd.concat(frames)
        df.to_parquet(file_address)
        return df
    else:
        return pd.read_parquet(file_address)
    
    
def ncbi_merge_all(datadir=DATA_DIR,file_address=DATA_DIR+'entrez_gene_raw_data_download.parquet',cache=True):
    if not cache or not os.path.isfile(file_address):    
        files=list(filter(lambda x: 'ncbi.parquet' in x,os.listdir(datadir)))
        frames=[pd.read_parquet(datadir+x) for x in tqdm(files)]
        df=pd.concat(frames)   
        
        for col in tqdm(df.columns):
            df.loc[df[col]=='',col]=np.nan

        for col in tqdm(df.columns):
            df.loc[df[col]=='[]',col]=np.nan   
            
        df.to_parquet(file_address)
        return df
    else:
        return pd.read_parquet(file_address)    
    
def uniprot_summary(output_file=DATA_DIR+'uniprot_raw_data_download_summary.txt',datadir=DATA_DIR):
    uniprot_data=uniprot_merge_all(datadir)
    
    text_file = open(output_file, "w")
    #A list of the exact path of each downloaded file (including on which machine).
    import time

    download_time_df=[]
    files=list(filter(lambda x: 'uniprot.parquet' in x,os.listdir(datadir)))
    for file in files:
        download_time_df.append({'organism':file.replace('_uniprot.parquet',''),
                                 'date of download':time.ctime(os.path.getmtime(datadir+file)),
                                'file address':datadir+file})
    text_file.write('\nuniprot download date per organism: \n')  
    text_file.write(pd.DataFrame(download_time_df).to_string())
    del download_time_df
    
    temp=pd.DataFrame()
    temp['non na']= uniprot_data.shape[0]-uniprot_data.isna().sum()   
    cols=uniprot_data.columns    
    res=[]
    for col in tqdm(cols):
        res.append({'nunique':uniprot_data[col].nunique(),'col':col})  
    temp['nunique']=pd.DataFrame(res)['nunique'].values.tolist()    
    
    text_file.write('\n\nuniprot non na and number of unique per organism: \n')  
    text_file.write(temp.to_string())     
    
    text_file.write('\n\nuniprot ids per organism: \n') 
    rev={x[1]['tax_id']:x[0] for x in crops_dict.items()}
    uniprot_data_organisms=uniprot_data.groupby('Organism ID')['Entry'].count()    
    #uniprot_data_organisms.index=[rev[x] for x in uniprot_data_organisms.index]
    text_file.write(uniprot_data_organisms.to_string())  
    text_file.write('\n\ntotal number of uniprot ids: '+str(uniprot_data['Entry'].nunique()))
    text_file.close()       
    
    
    
def ncbi_summary(output_file=DATA_DIR+'entrez_gene_raw_data_download_summary.txt',datadir=DATA_DIR):
    ncbi_data=ncbi_merge_all(datadir)
    
    text_file = open(output_file, "w")
    #A list of the exact path of each downloaded file (including on which machine).
    import time

    download_time_df=[]
    files=list(filter(lambda x: 'ncbi.parquet' in x,os.listdir(datadir)))
    for file in files:
        download_time_df.append({'organism':file.replace('_ncbi.parquet',''),
                                 'date of download':time.ctime(os.path.getmtime(datadir+file)),
                                 'file address':datadir+file})
        
    text_file.write('\n\nncbi download date per organism: \n')  
    text_file.write(pd.DataFrame(download_time_df).to_string())
    del download_time_df
    
    temp=pd.DataFrame()
    temp['non na']= ncbi_data.shape[0]-ncbi_data.isna().sum()   
    cols=ncbi_data.columns    
    res=[]
    for col in tqdm(cols):
        res.append({'nunique':ncbi_data[col].nunique(),'col':col})  
    temp['nunique']=pd.DataFrame(res)['nunique'].values.tolist()    
    
    text_file.write('\n\nncbi non na and number of unique per organism: \n')  
    text_file.write(temp.to_string())     
    
    text_file.write('\n\nncbi ids per organism: \n') 
    rev={x[1]['tax_id']:x[0] for x in crops_dict.items()}
    ncbi_data_organisms=ncbi_data.groupby('organism')['uid'].count()
    ncbi_data_organisms.index=[eval(x)['scientificname'] for x in ncbi_data_organisms.index]
    text_file.write(ncbi_data_organisms.to_string())  
    text_file.write('\n\ntotal number of ncbi ids: '+str(ncbi_data['uid'].nunique()))
    text_file.close()  
    
    
def exp_summary(output_file=DATA_DIR+'array_express_raw_data_download_summary.txt',datadir=DATA_DIR):
    exp_data=exp_merge_all(datadir)
    
    text_file = open(output_file, "w")
    #A list of the exact path of each downloaded file (including on which machine).
    import time

    download_time_df=[]
    files=list(filter(lambda x: 'exp.parquet' in x,os.listdir(datadir)))
    for file in tqdm(files):
        try:
            nunique=pd.read_parquet(datadir+file)['Gene ID'].nunique()
            download_time_df.append({'organism':file.replace('_exp.parquet',''),
                                 'date of download':time.ctime(os.path.getmtime(datadir+file)),
                                 'number of ids':nunique,
                                 'file address':datadir+file})
        except:
            print(file.replace('_exp.parquet',''))            
            download_time_df.append({'organism':file.replace('_exp.parquet',''),
                                 'date of download':time.ctime(os.path.getmtime(datadir+file)),
                                 'number of ids':0,
                                 'file address':datadir+file})          
    
    text_file.write('\n\narrayexpress download date per organism: \n')  
    text_file.write(pd.DataFrame(download_time_df).to_string())
    text_file.write('\n\ntotal number of genes: '+str(pd.DataFrame(download_time_df)['number of ids'].sum()))  
    del download_time_df    
    
    
def download_and_merge(cache=True,verbose=True,test=False):
    '''
    downloads (or verifies existence) db files for each organism
    merges to one big dataframe
    '''

    file_address=DATA_DIR+'gene_data_merged_all.parquet'
    

    uniprot_download_organisms(cache=cache)
    ncbi_download_organisms(cache=cache)
    exp_download_organisms(cache=cache)

    #read downloaded(merge organisms in each database)
    uniprot=uniprot_merge_all(cache=test)
    
    exp=exp_merge_all(cache=test)
#         uniprot=uniprot_merge_all(cache=cache)
#         ncbi=ncbi_merge_all(cache=cache)
#         exp=exp_merge_all(cache=cache)

    #merge expression with map to get entry per gene
    # first merge all mapping files for all organisms to one mapping file
    merge_mapping_all(cache=True)

    # gene_map=APP_DIR+'data/uni_to_ensemble.csv'
    gm=pd.read_csv(DATA_DIR+'uni_to_ensemble.csv')
    gm.rename(columns={'From':'Entry','To':'gid'},inplace=True)
    #gm.columns=['bla','Entry','bla2','gid']
    gm=gm[['Entry','gid']]
    gm['gid']=gm['gid'].astype(str).str.lower()
    exp['merge_']=exp.index.astype(str).str.lower()
    exp=pd.merge(exp,gm[['Entry','gid']],how='left',left_on='merge_',right_on='gid')
    del gm
    #merge expression with uniprot
    print('uniprot',uniprot.shape)
    uniprot=pd.merge(uniprot,exp,on='Entry',how='left')

    ncbi=ncbi_merge_all(cache=test)
    #merge uniprot with ncbi
    uniprot['Cross-reference (GeneID)']=uniprot['Cross-reference (GeneID)'].str.split(';').apply(lambda l: l[0] if type(l)==list else None).astype(str)
    ncbi['uid']=ncbi['uid'].astype(str)
    del exp
 
    merged=pd.merge(uniprot, ncbi, how='left', left_on='Cross-reference (GeneID)', right_on='uid')

#     merged.drop_duplicates(inplace=True)   

    if verbose:
        print('uniprot',uniprot.shape)
        print('ncbi',ncbi.shape)        

    del ncbi
    del uniprot

    merged=merged.drop_duplicates(subset=['Entry'])
    if verbose:    
        print('merged',merged.shape)    
    #save
    merged.drop(columns=['gid','merge_'],inplace=True)   
    merged.to_parquet(file_address)
    return merged

    
    
    
def gene_data_summary(output_file='gene_data_merged_all_summary.txt',
                    datadir=DATA_DIR,
                   verbose=True):
    if verbose:
        print('loading data')
    gene_data=download_and_merge(test=True)
    
    mydate = []
    today = datetime.date.today()
    mydate.append(today)

    if verbose:
        print('finished loading, summarizing')
    uniprot_summary()
    ncbi_summary()
    exp_summary()
    
    file_address=DATA_DIR+'gene_data_merged_all.parquet'
    
    to_write=[]
    output_files=[DATA_DIR+'uniprot_raw_data_download_summary.txt',
                 DATA_DIR+'entrez_gene_raw_data_download_summary.txt',
                DATA_DIR+'array_express_raw_data_download_summary.txt']
    for file in output_files:
        with open(file,'r') as f:
            data=f.read()
        to_write.append(data)
        if verbose:
            print('\n')
            print(file.replace(DATA_DIR,'').replace('_raw_data_download_summary.txt','')," summary:\n")
            print(data)    
    

    
    text_file = open(datadir+str(mydate[0])+'_'+output_file, "w")
    [text_file.write(x) for x in to_write]
    
    if verbose:
        print('\n\nsummary of merged: \n')
        print('total number of records in merged gene data: '+str(gene_data.shape[0]))
        print('total number of columns in merged gene data: '+str(gene_data.shape[1]))  
        print('date of merge:',time.ctime(os.path.getmtime(file_address)))
        
    text_file.write('\n\nlocation of merged file: '+DATA_DIR+'/gene_data_merged_all.parquet')
    text_file.write('\ntotal number of records in merged gene data: '+str(gene_data.shape[0]))
    text_file.write('\ntotal number of columns in merged gene data: '+str(gene_data.shape[1]))  
    text_file.write('\ndate of merge: '+time.ctime(os.path.getmtime(file_address))) 
    
    
    to_write='''
    import os
    if os.getcwd().endswith('notebooks'): os.chdir('../')
    from gene_discovery.public_data_download import *
    download_and_merge()    
    '''  
    text_file.write("\n\nexample use: \n%s" % to_write)    
  
    to_write='''
    import os
    if os.getcwd().endswith('notebooks'): os.chdir('../')
    from tests.test_public_data_download import *    
    test_download_and_merge()  
    '''  
    text_file.write("\nto run test: \n%s" % to_write)  
    
    text_file.close()        