# steps to start:
## git clone: git clone https://yourusername@bitbucket.org/amitshalev1/gene_discovery2.git



## gpu:
### nvidia-docker run -it -v $git_directory/:/app/ -v /mnt/amit_work/data_new/:/data/ -p 8888:8888 amitshalev/vco260_gpu bash
## cpu:
### nvidia-docker run -it -v $git_directory/:/app/ -v /mnt/amit_work/data_new/:/data/ -p 8888:8888 amitshalev/vco260_cpu bash

## download data and create summary:

### python
### from gene_discovery.public_data_download import *
### gene_data_summary()

## to run download and merge test:

### from tests.test_public_data_download import *
### test_download_and_merge()
