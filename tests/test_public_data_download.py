from gene_discovery.public_data_download import *

def test_uniprot_download_organism():
    '''
    download 2 proteins using the same query(limited to 2 proteins)
    compare with saved
    '''
    import requests
    import pandas as pd

    tax_id=3072
    url  = 'https://www.uniprot.org/uniprot/?query=organism:%s&columns=%s&format=tab&limit=2&offset=10'%(tax_id,columns)
    req = requests.get(url)    
    if req.status_code==200 and req.text!="":
        df=pd.read_table(StringIO(req.text),sep='\t',error_bad_lines=False)    
    assert pd.read_csv('tests/data/uniprot_available.csv').drop(columns=['Unnamed: 0']).equals(df)
    
    
def test_uniprot_download_organisms():
    '''
    what is needed to test?
    should i test multiproccessing
    '''
    assert True
    
    
    
def test_get_ncbi_gene_crop():
    '''
    download all genes for selected organism from gene db in ncbi
    '''
    db='gene'
    organism='sprus'
    tax_id=crops_dict[organism]['tax_id']    
    #print(directory)      
    query,webenv,id_list=ncbigethist(ncbigetcrop(db,tax_id))
    num=len(id_list)
    frames=[ncbigetsummary(db,query,webenv,x) for x in tqdm(np.arange(int(num/500)+2)*500)]
    temp=[]
    for i in range(len(frames)):
        if 'result' in list(frames[i].keys()):
            temp.append(pd.DataFrame(pd.DataFrame(frames[i])[:-3]['result'].values.tolist()))

    df2=pd.concat(temp)
    df=pd.read_parquet('tests/data/ncbi_sprus.parquet')
    df['uid']=df['uid'].astype(str)
    assert df2[['uid']].equals(df[['uid']]),"sprus is different, check if it was updated"    
    
    
def test_crop_array_express():
    '''
    downloads one experiment from array express and compares to saved
    '''
    organism='tomato'
    latin_name=crops_dict[organism]['latin_name'] 
    array_express1,entry_list=array_express(latin_name)
    'E-GEOD-64087' in entry_list
    p1='https://www.ebi.ac.uk/gxa/experiments-content/'
    #u2='/resources/ExperimentDownloadSupplier.Microarray/query-results'
    u2='/download/RNASEQ_MRNA_BASELINE'
    #p2='/download/RNASEQ_MRNA_DIFFERENTIAL'
    x='E-GEOD-64087'
    r=requests.get(p1+ x+ u2)
    gene_id=r.text.find('Gene ID')
    df=pd.read_table(StringIO(r.text[gene_id:]),sep='\t',error_bad_lines=False)
    assert pd.read_parquet('tests/data/exp_tomato.parquet').equals(df),"something is wrong"    
    
    
    
def test_download_and_merge():
    #Download the data
    gene_data=download_and_merge(test=True)
    assert os.path.isfile(DATA_DIR+'gene_data_merged_all.parquet'),"merged file is missing"
    assert gene_data.shape[0]>100000,"less the a 100000 records in merged gene data"
    #Very file cached. (file exist + can be opened)
    for organism in crops_dict.keys():
        for dataset in ['uniprot','ncbi','exp']:
            assert os.path.isfile(DATA_DIR+organism+'_'+dataset+'.parquet'),dataset+' data is missing for '+organism
            try:
                try:
                    df=pd.read_parquet(DATA_DIR+organism+'_'+dataset+'.parquet')
                    #df=pd.read_parquet(DATA_DIR+organism+'_'+dataset+'.parquet')
                    can_read=True
                except:
                    #handles empty expression dataframes
                    #df=pd.read_parquet(DATA_DIR+organism+'_'+dataset+'.parquet')
                    df=pd.read_csv(DATA_DIR+organism+'_'+dataset+'.parquet').empty

                    can_read=True                
            except:
                can_read=False
            assert can_read,dataset+' data is corrupted for '+organism +'   ' +DATA_DIR+organism+'_'+dataset+'.parquet'
    gene_data_shape=gene_data.shape[1]
    nunique=gene_data['Entry'].nunique()
    del gene_data
    gene_data_summary(verbose=True)
    
    cache=True
    uniprot=uniprot_merge_all(cache=cache)
    ncbi=ncbi_merge_all(cache=cache)
    exp=exp_merge_all(cache=cache)

    #check number of columns equals sum of each database columns
    #print(uniprot.shape[1],ncbi.shape[1],exp.shape[1],uniprot.shape[1]+ncbi.shape[1]+exp.shape[1],gene_data.shape[1])
    assert uniprot.shape[1]+ncbi.shape[1]+exp.shape[1]==gene_data_shape,"number of columns is unequal after merge"

    #check number of records is same as in uniprot
    print('number unique entries in uniprot: ',str(uniprot['Entry'].nunique()))
    print('number of unique entries in merged: ',str(nunique))
    assert uniprot['Entry'].nunique()==nunique,"number of records is unequal after merge"    