import requests
from imports import *


Entrez.email = 'xoiu@gmail.com'
uniprot_columns = 'id,citation,organism-id,genes(ALTERNATIVE),genes(PREFERRED),protein%20names'


@mem.cache
def explode_col(df, col, on='Entry', split_char=';'):
    df = df[[on, col]].copy().dropna()
    df[col] = df[col].str.split(split_char)
    return pd.DataFrame([(entry, val)  for entry, vals in tqdm(df.values) for val in vals], columns=[on, col])


@mem.cache
def requst_to_pandas(url, delimiter='\t'):
    result = requests.get(url).text
    result = [line.split('\t') for line in result.split('\n')]
    return pd.DataFrame(result[1:], columns=result[0])

def download_organism_uniprot_pubmeds(organism, columns=uniprot_columns, limit=1000, offset=0):
    url = f'https://www.uniprot.org/uniprot/?query=organism:{organism}&format=tab&limit={limit}&columns={columns}&offset={offset}'
    df = requst_to_pandas(url)
    return df

def download_organism_pubmeds_in_batches(organism, batch_size=10000, max_batches=20):
    dfs = pd.DataFrame([])
    batches = range(0, batch_size*(max_batches)+1, batch_size)
    for batch in tqdm(batches):
        df = download_organism_uniprot_pubmeds(organism, limit=batch_size, offset=batch)
        if df.shape[0] == 0: break
        dfs = pd.concat([dfs, df], ignore_index=True, sort=False)
    return dfs

@mem.cache
def get_all_genes_with_pubmeds(organisms = ORGANISM_IDS_IN_GENE_DATA):
    genes_with_pubmeds = pd.DataFrame([])
    for organism in organisms:
        organism_df = download_organism_pubmeds_in_batches(organism)
        organism_df = organism_df.replace('', np.nan)
        organism_df = organism_df.dropna(subset=['PubMed ID'])
        genes_with_pubmeds = pd.concat([genes_with_pubmeds, organism_df], sort=False, ignore_index=True)
        clear_output()
    genes_with_pubmeds = genes_with_pubmeds.replace('', np.nan)
    genes_with_pubmeds = genes_with_pubmeds.dropna(subset=['PubMed ID'])
    return genes_with_pubmeds


@mem.cache
def get_gene2pubmed(organisms = ORGANISM_IDS_IN_GENE_DATA):
    genes_with_pubmeds = get_all_genes_with_pubmeds(organisms)
    gene2pubmed = explode_col(genes_with_pubmeds[['Entry', 'PubMed ID', 'Organism ID']], 'PubMed ID')
    gene2pubmed = gene2pubmed.replace(' ', np.nan).dropna()
    return gene2pubmed

@mem.cache
def download_pubmed_abstracts(pmids):
    handle = Entrez.efetch(db="pubmed", id=pmids, rettype="medline",retmode="text")
    df = pd.DataFrame([article for article in Medline.parse(handle)])
    return df

@mem.cache
def get_pubmeds_dataframes_with_batches(pmids, batch_size=1000, verbose=True):
    batches = range(0, len(pmids)+1, batch_size)
    if verbose: batches = tqdm(batches)
    dfs = pd.DataFrame([])
    for batch in batches:
        batch_pmids = pmids[batch: batch+batch_size]
        df = download_pubmed_abstracts(batch_pmids)
        dfs = pd.concat([dfs, df], ignore_index=True, sort=False)
    return dfs

@mem.cache
def get_titles_bert_embedding(titles, verbose=True):
    from bert_embedder import bert_embedding
    return bert_embedding(titles, verbose = verbose)


def get_gentra_data_XY(ANNO_FILE, pubmed_titles_bert_embeddings):
    anno = pd.read_csv(ANNO_FILE).dropna()
    anno['PMID'] = anno['PMID'].astype(int).astype(str)
    anno = anno[anno['PMID'].isin(pubmed_titles_bert_embeddings.index)]
    
    X = pubmed_titles_bert_embeddings[pubmed_titles_bert_embeddings.index.isin(anno['PMID'])]
    X = X.loc[anno['PMID']].dropna().drop_duplicates()
    Y = anno['is_relevant'].values
    assert all(X.index == anno['PMID'])
    assert len(Y) == len(X)
    return X.values, Y


def get_gentra_model():
    from tensorflow.keras.layers import Input, Dense, Dropout, BatchNormalization
    from tensorflow.keras.models import Model
    inp = Input(shape=768)
    x = inp
    for i in range(5):
        x = BatchNormalization()(x)
        x = Dropout(0.2)(x)
        x = Dense(400, kernel_initializer='lecun_normal', activation='selu')(x)
    x = Dense(1, activation='sigmoid')(x)
    model = Model(inp, x)
    model.compile(optimizer='Adamax', loss='binary_crossentropy', metrics=['accuracy'])
    return model

def extract_keywords_with_yake(text, join_char='|'):
    custom_kwextractor = yake.KeywordExtractor(lan="en", n=5, dedupLim=0.9, dedupFunc='seqm', windowsSize=1, top=20, features=None)
    keywords = [k[0] for k in custom_kwextractor.extract_keywords(text)]
    if join_char != '':
        keywords = join_char.join(keywords)
    return keywords

@mem.cache
def get_trait_ontology(sentence_ = True):
      import requests
      import pandas as pd
      import numpy as np


      obo = requests.get('https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/to.obo')
      obo = str(obo.content, encoding='utf8')

      terms = obo.split('[Term]')[1:]
      all_keys = set()
      for term in terms:
        for line in term.split('\n'):
          try:
            key, val = line.split(': ')
            all_keys.add(key)

          except:pass

      obo_data = []
      for term in terms:
        term_data = {k : np.nan for k in all_keys}
        for line in term.split('\n'):
          try:
            key, val = line.split(': ')
            term_data[key] = val

          except:pass
        obo_data.append(term_data)
      df = pd.DataFrame(obo_data)
      df = df.dropna(subset=['name', 'def'])
      df = df.fillna('')
      df = df[df['is_obsolete'] != 'true']
      if sentence_:
        to = df[~(df['def']=='')]

        to['def_'] = to['def'].str.replace('"', '')
        to['def_'] = to['def_'].str.replace('\([A-Z]+:\d+\)', '')
        to['def_'] = to['def_'].str.replace('\[.+\]', '')

        to['name_'] = to['name'].str.replace(' trait', '')
        to['is_a_'] = [v[1] if len(v)>1 else v[0] for v in to['is_a'].str.split('! ')]
        to['is_a_'] = [' is a ' + v if len(v) >2 else '' for v in to['is_a_'].values]

        to['comment_'] = to['comment'].str.replace('\([A-Z]+:\d+\)', '')
        to['comment_'] = to['comment_'].str.replace('\[.+\]', '')

        to['sentence_'] = to['name_'] + to['is_a_'] +' .' + to['comment_'] +' defined as '+ to['def_'] + to['name_'] 
        to['sentence_'] = to['sentence_'].replace( ' .', '.')
        to['sentence_'] = to['sentence_'].replace('  ', ' ')
        to['sentence_'] = to['sentence_'].replace( '..', '')
        return to
      return df