from imports2 import *
from neural_network_1_architecture_sprint_code_2019_07_28 import *
import inspect
from functools import wraps

# TESTING SUITE ==================================
# ================================================ 
def test_decor(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        print('-'*100)
        print("testing:", f.__name__)
        return f(*args, **kwds)
    return wrapper
    
@test_decor
def test_entries_in_test_data_are_not_in_train(X_train, X_valid):
    try:
        assert len(X_train[X_train.index.isin(X_valid.index)]) == 0
        print('pass')
    except AssertionError:
        print('! Failed')

@test_decor
def test_all_traits_in_valid_are_in_train(Y_train, Y_valid):
    train_comparison = [trait for trait in Y_valid.index 
                       if not trait in Y_train.index]
    try:
        assert len(train_comparison) == 0, f"some traits in validation are not in train: {set(train_comparison)}"
        print('pass')
    except AssertionError:
        print('! Failed')

    
@test_decor 
def test_no_duplicated_data_in_validation(X_valid):
    n_duplicated = len(X_valid) - len(X_valid.drop_duplicates())
    if n_duplicated > 0:
        n_unique_duplicates = len(X_valid[X_valid.duplicated()].drop_duplicates())
    try:
        assert n_duplicated == 0, f"There are {n_duplicated} ({n_unique_duplicates} uniques) duplicated values in 'X_valid'"
        print('pass')
    except AssertionError:
        print('! Failed', f"There are {n_duplicated} ({n_unique_duplicates} uniques) duplicated values in 'X_valid'")

@test_decor
def test_no_all_zeros_data_points(X_train, X_valid):
    try:
        assert X_valid[X_valid.sum(axis=1) == 0].shape[0] == 0, 'all 0 features in X_valid'
        assert X_train[X_train.sum(axis=1) == 0].shape[0] == 0, 'all 0 features in X_train'
        print('pass')
    except AssertionError:
        print('! Failed')


@test_decor
def test_same_gene_data_in_test_and_validation(X_train, X_valid):
    X_train_without_duplicates = X_train.drop_duplicates()
    X_valid_without_duplicates = X_valid.drop_duplicates()
    
    valid_set = set([tuple(v) for v in X_valid_without_duplicates.values])
    train_set = set([tuple(v) for v in X_train_without_duplicates.values])
    
    intersect = train_set.intersection(valid_set)
    try:
        assert len(intersect) == 0, f"some gene_data ({len(intersect)}) matches exactly between train and validation"
        print('pass')
    except AssertionError:
        print('! Failed:', f"some gene_data ({len(intersect)}) matches exactly between train and validation")

@test_decor
def test_not_all_columns_are_same_values(X_train, X_valid):
    con = pd.concat([X_train, X_valid])
    len_same_columns = len(con.columns[con.nunique()  < 2])
    try:
        assert len_same_columns == 0, f"{len_same_columns} are all empty"
        print('pass')
    except AssertionError:
        print ('! Failed:', f"{len_same_columns} are all empty")


        
# ===========================================================================        
def test_data(X_train, Y_train, X_valid, Y_valid):
    test_entries_in_test_data_are_not_in_train(X_train, X_valid)
    test_all_traits_in_valid_are_in_train(Y_train, Y_valid)
    test_no_duplicated_data_in_validation(X_valid)
    test_no_all_zeros_data_points(X_train, X_valid)
    test_same_gene_data_in_test_and_validation(X_train, X_valid)
    test_not_all_columns_are_same_values(X_train, X_valid)