from imports2 import *
from neural_network_1_architecture_sprint_code_2019_07_28 import *

# data fix suite ============================================
def remove_duplicates_from_validation_data(X_valid, Y_valid):
    X_valid, Y_valid = X_valid.copy(), Y_valid.copy()
    X_valid['idx'] = list(range(X_valid.shape[0]))
    X_valid = X_valid.drop_duplicates(subset = X_valid.columns[:-1])
    Y_valid = Y_valid.iloc[X_valid['idx'].values]
    del X_valid['idx']
    assert X_valid.shape[0] == Y_valid.shape[0]
    return X_valid, Y_valid


def remove_all_zeros_rows(X_train, Y_train, X_valid, Y_valid):
    X_train, Y_train, X_valid, Y_valid = X_train.copy(), Y_train.copy(), X_valid.copy(), Y_valid.copy()
    
    X_valid['idx'] = list(range(X_valid.shape[0]))
    X_train['idx'] = list(range(X_train.shape[0]))
    
    X_valid = X_valid[X_valid[X_valid.columns[0:-1]].sum(axis=1) != 0]
    X_train = X_train[X_train[X_train.columns[0:-1]].sum(axis=1) != 0]
    
    Y_valid = Y_valid.iloc[X_valid['idx'].values]
    Y_train = Y_train.iloc[X_train['idx'].values]
    
    del X_valid['idx']
    del X_train['idx']
    
    assert X_valid.shape[0] == Y_valid.shape[0]
    assert X_train.shape[0] == Y_train.shape[0]
    
    return X_train, Y_train, X_valid, Y_valid


def fix_not_all_traits_in_valid_are_in_train(X_train, Y_train, X_valid, Y_valid):
    X_train, Y_train, X_valid, Y_valid = X_train.copy(), Y_train.copy(), X_valid.copy(), Y_valid.copy()
    
    train_comparison = [trait for trait in Y_valid.index  if not trait in Y_train.index]
    Y_valid['idx'] = list(range(len(Y_valid)))
    with_comparison_idx    = Y_valid[ Y_valid.index.isin(train_comparison)]['idx'].values
    without_comparison_idx = Y_valid[~Y_valid.index.isin(train_comparison)]['idx'].values
    del Y_valid['idx']
    
    #move these indexes from test to train
    X_train = pd.concat([X_train, X_valid.iloc[with_comparison_idx]])
    Y_train = pd.concat([Y_train, Y_train.iloc[with_comparison_idx]])
    
    X_valid = X_valid.iloc[without_comparison_idx]
    Y_valid = Y_valid.iloc[without_comparison_idx]
    
    return X_train, Y_train, X_valid, Y_valid


def get_tuple_lists_from_df(df):
    return [tuple(v) for v in tqdm(df.values, leave=True)]

def remove_data_leakage_from_Train_to_valid(X_train, Y_train, X_valid, Y_valid):
    X_train, Y_train, X_valid, Y_valid = X_train.copy(), Y_train.copy(), X_valid.copy(), Y_valid.copy()
    
    valid_list = get_tuple_lists_from_df(X_valid)
    train_set = set(get_tuple_lists_from_df(X_train.drop_duplicates()))
    intersect = train_set.intersection(set(valid_list))
    not_intersected = [idx for idx, tup in enumerate(valid_list) if not tup in intersect]
    
    X_valid = X_valid.iloc[not_intersected]
    Y_valid = Y_valid.iloc[not_intersected]
    assert X_valid.shape[0] == Y_valid.shape[0]
    
    return X_train, Y_train, X_valid, Y_valid

def fix_data_problems(from_date = '2019_07_25', save_fixed = True, fix_date = '2019_07_30'):
    print('loading data')
    X_train, Y_train, X_valid, Y_valid = data_loading(from_date)
    print('removing duplicates')
    X_valid, Y_valid = remove_duplicates_from_validation_data(X_valid, Y_valid)
    print('removing all zeros rows')
    X_train, Y_train, X_valid, Y_valid = remove_all_zeros_rows(X_train, Y_train, X_valid, Y_valid)
    print('fixing not all traits in valid are in train')
    X_train, Y_train, X_valid, Y_valid = fix_not_all_traits_in_valid_are_in_train(X_train, Y_train, X_valid, Y_valid)
    print('fixing data leakage')
    X_train, Y_train, X_valid, Y_valid = remove_data_leakage_from_Train_to_valid(X_train, Y_train, X_valid, Y_valid)
    print('data fixed!')
    if save_fixed:
        print('saving...')
        X_train.to_parquet(f'{DATA_DIR}{fix_date}_X_train_gene_data.parquet')
        Y_train.to_parquet(f'{DATA_DIR}{fix_date}_Y_train_trait_embedding.parquet')
        X_valid.to_parquet(f'{DATA_DIR}{fix_date}_X_validation_gene_data.parquet')
        Y_valid.to_parquet(f'{DATA_DIR}{fix_date}_Y_validation_trait_embedding.parquet')
        print('saved')
    return X_train, Y_train, X_valid, Y_valid