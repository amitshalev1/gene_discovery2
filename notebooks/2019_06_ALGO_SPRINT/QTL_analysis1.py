from imports2 import *
from neural_network_1_architecture_sprint_code_2019_07_28 import *
from sklearn.metrics.pairwise import cosine_distances, euclidean_distances

NUMERIC_SCALED_MERGED_FILE_NAME = f'{DATA_DIR}2019_07_24_GENE_DATA_numeric_merged_large.parquet'


# if not 'genemap' in globals():
#     genemap = generate_gene_to_genome_map()
#     gene_data = pd.read_parquet(NUMERIC_SCALED_MERGED_FILE_NAME)
    
# # ---------------------------------------------------------
# if not 'model' in globals():
#     FILE_PREFIX = '2019_07_experiments/2019_07_30_train_large_network_5'
#     MODEL_FILE = f'{FILE_PREFIX}.h5'
#     ARCHITECTURE = [3000] * 5
#     LOSS = 'mse'
#     OPTIMIZER = 'adam'
#     model = Gene_to_trait_model(layers_size = ARCHITECTURE, optimizer = OPTIMIZER, loss = LOSS)
#     model.load_weights(MODEL_FILE)
    
    
# -------------------------
def get_genes_in_virtual_QTL(Entry:"uniprot_id", genemap = None, QTL_size:int = 10**6, n_genes:int = None):
    '''returns a list of genes around specific uniprot Entry. either with baspairs "QTL_size" or n genes '''
    pos = genemap[genemap['Entry'] == Entry]
    if len(pos) == 0: return []
    
    org, chromosome, chrstart = pos.values[0][1:4]
    
    chrom = (genemap[ (genemap['Organism ID'] == org) 
                    & (genemap['chromosome']  == chromosome)]
             .sort_values('chrstart')).copy()
    
    if n_genes == None:
        qtl_start = max((chrstart - QTL_size / 2, 0))
        qtl_end   = chrstart + QTL_size / 2
    
        qtl = chrom[  (chrom['chrstart'] >= qtl_start) 
                    & (chrom['chrstop']  <= qtl_end)]
        return chrom, qtl
    else: # use n_genes and not chromose bp sizes...
        gene_pos = chrom['Entry'].values.tolist().index(Entry)
        half_n_genes = int(n_genes /  2)
        return chrom, chrom.iloc[gene_pos - half_n_genes: gene_pos + half_n_genes]
    

def grade_genes_to_trait(qtl:'genes_df', trait:str, model, gene_data, target_gene=None):
    entries = qtl['Entry'].values
    X = gene_data[gene_data.Entry_0.isin(entries)]
    predicted = model.predict(X.values[:, 1:])
    train_embedding = emb_map[emb_map.index == trait]
    cos_dist = cosine_distances(predicted, train_embedding).ravel()
    col_name = f'cos_dist : {trait}'
    qtl[col_name] = cos_dist
    qtl = qtl.sort_values(col_name)
    
    if not target_gene is None:
        position_in_list = qtl# qtl.drop_duplicates(subset=['chrstart'])
        position_in_list = position_in_list['Entry'].values.tolist().index(target_gene)
        return qtl, position_in_list
    
    return qtl


def visualize_qtl_to_trait(target_gene, genemap, trait, model, gene_data, QTL_size=10**6):
    chrom, qtl = get_genes_in_virtual_QTL(target_gene, genemap=genemap, QTL_size=QTL_size)
    sorted_qtl, position_in_list = grade_genes_to_trait(qtl, trait, model, gene_data, target_gene)
    sorted_qtl = sorted_qtl.copy()
    
    sorted_qtl['relevance'] = 1 - sorted_qtl[f'cos_dist : {trait}']
    sorted_qtl['relevance'] = sorted_qtl['relevance'] - sorted_qtl['relevance'].min()
    gene = sorted_qtl[sorted_qtl['Entry'] == target_gene]

    plt.figure(figsize=[20, 4])
    plt.bar(sorted_qtl['chrstart'], sorted_qtl['relevance'], width=1000)

    gene_color = 'red'
    plt.bar(gene['chrstart'], gene['relevance'], width=2000, color=gene_color)
    entry, org, chro, cstart, cstop, cos_dist, rel = gene.values[0]
    plt.text(cstart, rel, target_gene, color=gene_color)


    plt.xlabel(f'QTL (on chr. {sorted_qtl.chromosome.values[0]}, organism = {org}); N genes = {len(sorted_qtl)}')
    plt.ylabel(f'trait relevnce (1-distance)')
    
    message = f'gene {target_gene} is in position {position_in_list} out of {len(sorted_qtl)} genes on the QTL'
    plt.title(f'inside QTL distance map to the trait {trait}\n {message}')
    return sorted_qtl, position_in_list