import tensorflow as tf
#tf.disable_v2_behavior()
from imports2 import *
from tensorflow.keras.layers import Input, Dense, Add, BatchNormalization, Dropout, LeakyReLU
from tensorflow.keras.models import Model
from tensorflow.keras.losses import cosine_proximity
from tensorflow.keras.losses import logcosh
from tensorflow.losses import huber_loss
from tensorflow.keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

from keras_lamb import LAMBOptimizer
import keras
from clr2 import LRFinder, OneCycleLR
from sklearn.neighbors import NearestNeighbors

INP_SIZE, OUT_SIZE = 7576, 768
LAYERS_SIZE = [1000, 1000, 1000]


# -------------------------------
def data_loading(from_date = '2019_07_25'):
    X_train = pd.read_parquet(f'{DATA_DIR}{from_date}_X_train_gene_data.parquet')
    Y_train = pd.read_parquet(f'{DATA_DIR}{from_date}_Y_train_trait_embedding.parquet')
    X_valid = pd.read_parquet(f'{DATA_DIR}{from_date}_X_validation_gene_data.parquet')
    Y_valid = pd.read_parquet(f'{DATA_DIR}{from_date}_Y_validation_trait_embedding.parquet')
    return X_train, Y_train, X_valid, Y_valid


# -------------------------------------------



def dense_norm_layer(x:'kerasLayer', size:int, batchnorm:bool=True, drop:float=0.3):
    'connect a dense layer, initilized with he_normal and LeakyReLU actication. optinal batchNorm and dropout'
    x = Dense(size, kernel_initializer='he_normal', activation=LeakyReLU())(x)
    if batchnorm: x = BatchNormalization()(x)
    if drop > 0: x = Dropout(drop)(x)    
    return x

def layers_series(x:'kerasLayer', sizes:list, batchnorm:bool=True, drop:float=0.3):
    '''generates a series of layers (dense) based on a list of layers with specific sizes'''
    for size in sizes: x = dense_norm_layer(x, size, batchnorm, drop)
    return x

def add_skip_branch_if_True(x:'layer', inp:'inputLayer', layer_size, skip_branch=True):
    if skip_branch:
        x = Add()([x, dense_norm_layer(inp, layer_size)])
    return x

def Gene_to_trait_model(inp_size:int = INP_SIZE, 
                        layers_size:list = LAYERS_SIZE, 
                        out_size:int = OUT_SIZE,
                        skip_branch:bool = True,
                        optimizer = 'adam',
                        loss='mse'):
    
    '''generate a keras model for trainign gene to trait map'''
    inp = x = Input(shape=(inp_size,))
    
    x = layers_series(x, layers_size)
    x = add_skip_branch_if_True(skip_branch=skip_branch, x = x, inp = inp, layer_size = layers_size[-1])
    out = dense_norm_layer(x, out_size, False, 0)
    
    model = Model(inp, out, name='gene_to_trait')
    model.compile(loss=loss, optimizer=optimizer)
    return model

def find_lr_cycle(x, y,
                  batch_size = 1024,
                  minimum_lr = 0.005,
                  maximum_lr = 50.0,
                  optimizer='sgd', 
                  loss='mse'
                 ):


    num_samples = len(x)
    lr_callback = LRFinder(num_samples, batch_size, minimum_lr, maximum_lr, 
                          lr_scale='exp', save_dir='modeling',
                          stopping_criterion_factor = None)
    model = Gene_to_trait_model(optimizer=optimizer, loss=loss)
    model.fit(x, y, epochs=1, batch_size=batch_size, callbacks=[lr_callback])
    clear_output()
    lr_callback.plot_schedule()
    

    

# ----------------------------------------------------    
#  Code to evaluate the model ...

emb_map = pd.read_parquet(TRAIT_ONTOLOGY_BERT_EMBEDDING_FILE)

def embedding_to_traits(embedding, 
                        n_neigh = 5, 
                        emb_map=emb_map, 
                        show_dist=False, 
                        metric='cosine'):
    
    nn = NearestNeighbors(n_neighbors=n_neigh, metric=metric).fit(emb_map.values)
    distances, idxs = nn.kneighbors(embedding)
    ontologies = emb_map.index.values[idxs]
    if not show_dist: return ontologies
    return [list(zip(on, distances[i])) for i, on in enumerate(ontologies)]

def get_trait_in_top_n(trait, predicted_list, n_neigh = 10):
    '''get a list of predicted trait and claculate if a trait is in each position in the list'''
    results = [int(trait in predicted_list[0:n])
               for n in range(1, n_neigh)] 
    results += [int(trait in predicted_list)]
    return results
        

def evaluate_model1(model, X_valid, validation_gene_trait, n_neigh=10, metric='cosine', emb_map=emb_map):
    '''evaluate how good a model in predicting trait in top N traits'''
    pred = model.predict(X_valid.values)
    pred_traits = embedding_to_traits(pred, n_neigh=n_neigh, emb_map=emb_map)
    valid_to_pred = zip(validation_gene_trait.values, pred_traits)
    
    results = [[gene, to] + get_trait_in_top_n(to, predicted_list, n_neigh)
               for (gene, to), predicted_list 
               in tqdm(valid_to_pred, leave=False)
               ]
    
    results = pd.DataFrame(results, columns=['Entry', 'TO'] + [f'in_top_{i}' for i in range(1, n_neigh+1)])
    return results

def evaluate_model1_accuracy(results):
    res = results[results.columns[2:]]
    return res.sum() / res.shape[0] * 100