import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import os
import sys
import joblib
from IPython.display import clear_output
#from Bio import Medline, Entrez
import unicodedata
import functools

norm = functools.partial(unicodedata.normalize, "NFKD")

#import yake

sys.path.append(os.getcwd())
sys.path.append('/mnt/wrkdir/gene_discovery2/')
sys.path.append('/mnt/wrkdir/gene_discovery2/data/')
sys.path.append('/mnt/wrkdir/vco260/')
sys.path.append('/mnt/wrkdir/gene_discovery2/notebooks/2019_06_ALGO_SPRINT/bert_keras_repo')

#from gentra_bert_annotate3_jupyter import *

DATA_PATH = '/mnt/wrkdir/gene_discovery2/data/'
DATA_DIR = '/mnt/wrkdir/gene_discovery2/data/'
ANNO_FILE = '/mnt/wrkdir/vco260/data/2019_05_10_pubmed_titles_annotations_1.csv'
RELEVANT_PUBMEDS = f'{DATA_PATH}2019_06_22_pubmed_articles_filtered_by_relevance_using_bert.csv'
RELEVANT_PUBMEDS_YAKE = f'{DATA_PATH}2019_06_15_relevant_pubmed_articles_with_YAKE_keywords.csv'
TRAINING_PATH = '/mnt/wrkdir/gene_discovery2/data/training_data/'

ANNOTATED_PATH = f'{TRAINING_PATH}annotated_22_07.csv'
rice_gene_data = f'{TRAINING_PATH}rice_gene_data.parquet'
corn_gene_data = f'{TRAINING_PATH}corn_gene_data.parquet'
arabidopsis_gene_data =f'{TRAINING_PATH}arabidopsis_phenotype_gene_data.parquet'
gene_data_of_orthologs = f'{TRAINING_PATH}gene_data_of_orthologs.parquet'
#NUMERIC_MERGED_FILE_NAME = f'{DATA_DIR}gene_data_merged_all_NUMERIC.parquet'
#NUMERIC_SCALED_MERGED_FILE_NAME = f'{DATA_DIR}gene_data_merged_all_NUMERIC_SCALED.parquet'
TRAIT_ONTOLOGY_BERT_EMBEDDING_FILE = f'{DATA_DIR}_trait_ontology_definitions_sci_bert_embedding.parquet'

NUMERIC_SCALED_MERGED_FILE_NAME = f'{DATA_DIR}2019_07_24_GENE_DATA_numeric_merged_large.parquet'

trait_ontology_bert_embeding_file = f'{DATA_DIR}_trait_ontology_definitions_sci_bert_embedding.parquet'

ORGANISM_IDS_IN_GENE_DATA = [ 4577,  3330,  3847,  4521,  4113,  3702,  4072,  4530,  3635,
       39947,  4081,  3332,  4565,  3712,  4111,  3659,  3654,  3656,
        4679]
mem = joblib.Memory('cache', verbose=False)

trait_adjusments = {'100-seed weight' : 'seed weight',
 '1000-seed weight' : 'seed weight',
 'ADP glucose pyrophosphorylase activity trait' : 'enzyme activity trait',
 'CMS' : 'male sterility',
 'NCLB area under disease progress curve' : 'disease resistance',
 'apiculus color' : 'lemma apiculus color',
 'biological process related trait' : 'biological process trait',
 'blast disease resistance' : 'fungal disease resistance',
 'brassinosteroid  sensitivity' : 'brassinosteroid sensitivity',
 'chlorophyll content in an extract' : 'chlorophyll content',
 'chlorophyll-a content' : 'chlorophyll content',
 'chlorophyll-b content' : 'chlorophyll content',
 'cooking or brewing quality' : 'cooking quality trait',
 'copper content' : 'copper content trait',
 'crop damage resistance' : 'biotic stress trait',
 'enzymatic activity and protein content related trait' : 'enzyme activity trait',
 'enzyme activity' : 'enzyme activity trait',
 'false smut disease resistance' : 'fungal disease resistance',
 'fat and essential oil composition related' : 'fat and essential oil content',
 'glutamine synthetase content' : 'enzyme activity trait',
 'kernel weight' : 'seed weight',
 'leaf development' : 'leaf growth and development trait',
 'metabolite content related trait' : 'metabolite content trait',
 'milled rice' : 'milled rice yield',
 'mineral and ion content related trait' : 'mineral and ion content trait',
 'molybdenum concentration' : 'molybdenum content',
 'oxidative stress tolerance' : 'oxidative stress',
 'physiological process related trait' : 'biological process trait',
 'plant embryo development stage' : 'plant embryo development trait',
 'plantÖ³â\x80\x9a cell morphologyÖ³â\x80\x9a trait' : 'plant cell morphology trait',
 'quality trait' : 'plant quality trait',
 'relative shoot dry weight' : 'shoot dry weight',
 'relative total dry weight' : 'plant dry weight',
 'relative water content' : 'leaf relative water content',
 'reproductive development' : 'reproductive homeotic development trait',
 'reproductive homeotic developme' : 'reproductive homeotic development trait',
 'root size' : 'root weight',
 'seed composition based quality trait' : 'seed quality trait',
 'seed dormancy' : 'seed growth and development trait',
 'seed viability' : 'seed quality trait',
 'shoot branching' : 'stem morphology trait',
 'shoot potassium content' : 'shoot system potassium content',
 'shoot system yield trait' : 'yield trait',
 'shoot weight' : 'shoot fresh weight',
 'shootÂ\xa0apicalÂ\xa0meristem development' : 'shoot apical meristem development',
 'stature or vigor trait' : 'plant vigor trait',
 'stem rot disease resistance' : 'fungal disease resistance',
 'stover organic matter digestability' : 'stover organic matter digestibility',
 'tiller angle' : 'shoot axis morphology trait',
 'tolerant to freezing' : 'cold tolerance',
 'total biomass yield' : 'yield trait',
 'vigor related trait' : 'plant vigor trait',
 'viral resistance trait' : 'viral disease resistance',
 'zinc content': 'zinc content trait',
 'biotic stress' : 'biotic stress trait',
 'nutrient sensitivity ': 'nutrient sensitivity',
 'salinity tolerance' : 'salt tolerance',
 'sheath blight disease resistance' : 'bacterial blight disease resistance',
 'inflorescence length': 'inflorescence morphology trait',
 'ination rate' : 'germination rate'}


metdata_cols = ['Entry',
                 'Gene names',
                 'Cross-reference (GeneID)',
                 'Cross-reference (Gramene)',
                 'Cross-reference (ExpressionAtlas)',
                 'chrstart',
                 'description',
                 'genomicinfo',
                 'locationhist',
                 'name',
                 'otheraliases',
                 'otherdesignations',
                 'uid',
                 'summary',
                 'geneweight',
                 'Temperature dependence',
                 'Cross-reference (Genevisible)',
                 'Cross-reference (PANTHER)',
                 'pH dependence',
                 'Kinetics',
                'Catalytic activity',
                'Developmental stage',  # 'Developmental stage'  => very hard to engineer - need to do text analysis (?!)
                'Cross-reference (BioCyc)',
                'chrsort',
                'Organism ID',
                'geneticsource',
                'status',
                'currentid',
                'Induction.1',
               ]

to_engineer = [('Function [CC]' , ['FUNCTION: ', '{.+}'], '', 30),
               ('Keywords',       [],             ';', 30),
               ('Features', ['\(\d+\)'], ';', 30),
               ('Active site', ['{.+}', 'ACT_SITE \d+ \d+ ','\.'], '', 30),
               ('Binding site', ['{.+}', 'BINDING \d+ \d+ ','\.', '; via .+'], '', 30),
               ('Calcium binding', [ 'CA_BIND \d+ \d+ ', '\d\.', '\.',' ', ';;'], ';', 30),
               ('DNA binding', ['DNA_BIND \d+ \d+ ', '\{.+\}', '\.'], '', 30),
               ('Activity regulation', ['ACTIVITY REGULATION: ', '\{.+\}', '\.'], '', 30),
               ('Cofactor', ['COFACTOR: ', '\{.+\}', '\.', '; Xref=.+', 'Name=', 'Note='], '', 30),
               ('EC number', [], ';', 30),
               ('Metal binding', ['METAL \d+ \d+ ', '\.', ' \d', '\{.+\}', '[ \t]+$'], ';', 30),
               ('Nucleotide binding', ['NP_BIND \d+ \d+',  '\{.+\}', '\.', ' \d'], ';', 30),
               ('Pathway', ['PATHWAY: ', '\{.+\}', 'step \d/\d', '\.'], ';', 30),
               ('Rhea Ids', [], ';', 30),
               ('Site', ['SITE \d+ \d+ ','\{.+\}', '\.'], ';', 30),
               ('Tissue specificity', ['TISSUE SPECIFICITY: ','Expressed in ', '\{.+\}', '[ \t]+$', '  '], '.', 30),
               ('Induction', ['\{.+\}', 'INDUCTION: '], '.', 30),
               ('Disruption phenotype', ['DISRUPTION PHENOTYPE: ', ' under normal growth conditions', 'ity', 'nic'], '.', 10),
               ('Mutagenesis', ['MUTAGEN \d+ \d+ ', '.+->+.:', '\{.+\}', '\.', '\d+\%', 'Abolishes', 'activity', 'reduction', 'function'], ';', 4),
               ('Cross-reference (UniPathway)', [';UER\d+'], ',', 30),
               ('Cross-reference (OrthoDB)', [], '', 100),
               ('Cross-reference (Pfam)', [], ';', 300),
               ('organism', ["{'scientificname': '", "', 'commonname'.+"], '', 10)]


def fix_organism_name(name):
        try:
            if name.startswith('{'):
                name = eval(name)['scientificname']
            return name
        except:
            return None
        
def merge_and_prepare_training_data(columns=['Entry', 'TO', 'organism']):
    
    dropna_subset = ['Entry', 'TO']
    
    annotated = pd.read_csv(ANNOTATED_PATH)
    del annotated['organism']
    annotated = annotated.rename(columns={'Organism' : 'organism'})
    annotated = annotated[columns].dropna(subset = dropna_subset)
    annotated['type'] = 'annotated'
    
    associated_rice = pd.read_parquet(rice_gene_data)[columns].dropna(subset = dropna_subset)
    associated_rice['type'] = 'association'
    
    associated_corn = pd.read_parquet(corn_gene_data)[columns].dropna(subset = dropna_subset)
    associated_corn['type'] = 'association'
    
    orthologs = pd.read_parquet(gene_data_of_orthologs)[columns].dropna(subset = dropna_subset)
    orthologs['type'] = 'orthologs'
    
    phenotyped = pd.read_parquet(arabidopsis_gene_data)[columns].dropna(subset = dropna_subset)
    phenotyped['type'] = 'phenotyped'
    
    
    merged = pd.concat([annotated, associated_rice, associated_corn, orthologs, phenotyped], ignore_index=True)
    merged = merged.drop_duplicates(subset=columns)
    
    # fix problematic organisms:
    if 'organism' in merged.columns:
        merged['organism'] = merged['organism'].map(fix_organism_name)
    
    # fix problematic traits:
    merged['TO'] = merged['TO'].apply(norm)
    merged['TO'] = merged['TO'].apply(lambda trait: trait_adjusments.get(trait, trait))
    merged['Entry'] = merged['Entry'].str.strip()
    
    return merged

def get_available_trait_ontologies():
    to_emb = pd.read_parquet(trait_ontology_bert_embeding_file)
    available_to = to_emb.index.values
    return available_to


def get_gene_data():
    return pd.read_parquet(NUMERIC_SCALED_MERGED_FILE_NAME)


@mem.cache
def get_all_parquets_columns(filenames):
    columns = []
    for file in tqdm(filenames):
        columns += pd.read_parquet(file).columns.tolist()
    return set(columns)

@mem.cache
def merge_column_across_files(files, column):
    dfs = [pd.read_parquet(file, columns=[column])
          for file in tqdm(files, leave=False)]
    return pd.concat(dfs)


def chrstop_from_genomicinfo(genomicinfo:str):
        try: return int(eval(genomicinfo)[0]['chrstop'])
        except: return -1
        
def generate_gene_to_genome_map(out_file = f'{DATA_DIR}all_genes_mapping2.parquet', use_saved = True):
    if use_saved and os.path.isfile(out_file):
        return pd.read_parquet(out_file)
    
    organism_based_merged_data = [DATA_DIR + f for f in os.listdir(DATA_DIR) if f.endswith('_merged.parquet')]
    files = [f for f in organism_based_merged_data if not 'total' in f]
    df = pd.DataFrame()
    for col in ['Entry', 'Organism ID', 'chromosome', 'chrstart', 'genomicinfo']:
        df[col] = merge_column_across_files(files, col)[col]
    df['chrstop'] = df['genomicinfo'].map(chrstop_from_genomicinfo)
    del df['genomicinfo']
    df = df.dropna()
    
    def make_int(val):
        try: return int(val)
        except: return None    
    df['chrstart'] = df['chrstart'].apply(make_int)
    df = df.dropna()
    df['chrstart'] = df['chrstart'].astype(int)
    df.to_parquet(out_file)
    return df