import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import os
import sys
import joblib
from IPython.display import clear_output
from Bio import Medline, Entrez
#import yake

sys.path.append(os.getcwd())
sys.path.append('/mnt/wrkdir/gene_discovery2/')
sys.path.append('/mnt/wrkdir/gene_discovery2/data/')
sys.path.append('/mnt/wrkdir/vco260/')
sys.path.append('/mnt/wrkdir/gene_discovery2/notebooks/2019_06_ALGO_SPRINT/bert_keras_repo')

#from gentra_bert_annotate3_jupyter import *

DATA_PATH = '/mnt/wrkdir/gene_discovery2/data/'
DATA_DIR = '/mnt/wrkdir/gene_discovery2/data/'
ANNO_FILE = '/mnt/wrkdir/vco260/data/2019_05_10_pubmed_titles_annotations_1.csv'
RELEVANT_PUBMEDS = f'{DATA_PATH}2019_06_22_pubmed_articles_filtered_by_relevance_using_bert.csv'
RELEVANT_PUBMEDS_YAKE = f'{DATA_PATH}2019_06_15_relevant_pubmed_articles_with_YAKE_keywords.csv'
TRAINING_PATH = '/mnt/wrkdir/gene_discovery2/data/training_data/'

ANNOTATED_PATH = f'{TRAINING_PATH}annotated_genes_traits_2019_07_02.csv'
rice_gene_data = f'{TRAINING_PATH}rice_gene_data.parquet'
corn_gene_data = f'{TRAINING_PATH}corn_gene_data.parquet'
gene_data_of_orthologs = f'{TRAINING_PATH}gene_data_of_orthologs.parquet'
NUMERIC_MERGED_FILE_NAME = f'{DATA_DIR}gene_data_merged_all_NUMERIC.parquet'
NUMERIC_SCALED_MERGED_FILE_NAME = f'{DATA_DIR}gene_data_merged_all_NUMERIC_SCALED.parquet'
TRAIT_ONTOLOGY_BERT_EMBEDDING_FILE = f'{DATA_DIR}_trait_ontology_definitions_sci_bert_embedding.parquet'

trait_ontology_bert_embeding_file = f'{DATA_DIR}_trait_ontology_definitions_sci_bert_embedding.parquet'

ORGANISM_IDS_IN_GENE_DATA = [ 4577,  3330,  3847,  4521,  4113,  3702,  4072,  4530,  3635,
       39947,  4081,  3332,  4565,  3712,  4111,  3659,  3654,  3656,
        4679]
mem = joblib.Memory('cache', verbose=False)

trait_adjusments = {'100-seed weight' : 'seed weight',
 '1000-seed weight' : 'seed weight',
 'ADP glucose pyrophosphorylase activity trait' : 'enzyme activity trait',
 'CMS' : 'male sterility',
 'NCLB area under disease progress curve' : 'disease resistance',
 'apiculus color' : 'lemma apiculus color',
 'biological process related trait' : 'biological process trait',
 'blast disease resistance' : 'fungal disease resistance',
 'brassinosteroid  sensitivity' : 'brassinosteroid sensitivity',
 'chlorophyll content in an extract' : 'chlorophyll content',
 'chlorophyll-a content' : 'chlorophyll content',
 'chlorophyll-b content' : 'chlorophyll content',
 'cooking or brewing quality' : 'cooking quality trait',
 'copper content' : 'copper content trait',
 'crop damage resistance' : 'biotic stress trait',
 'enzymatic activity and protein content related trait' : 'enzyme activity trait',
 'enzyme activity' : 'enzyme activity trait',
 'false smut disease resistance' : 'fungal disease resistance',
 'fat and essential oil composition related' : 'fat and essential oil content',
 'glutamine synthetase content' : 'enzyme activity trait',
 'kernel weight' : 'seed weight',
 'leaf development' : 'leaf growth and development trait',
 'metabolite content related trait' : 'metabolite content trait',
 'milled rice' : 'milled rice yield',
 'mineral and ion content related trait' : 'mineral and ion content trait',
 'molybdenum concentration' : 'molybdenum content',
 'oxidative stress tolerance' : 'oxidative stress',
 'physiological process related trait' : 'biological process trait',
 'plant embryo development stage' : 'plant embryo development trait',
 'plantÖ³â\x80\x9a cell morphologyÖ³â\x80\x9a trait' : 'plant cell morphology trait',
 'quality trait' : 'plant quality trait',
 'relative shoot dry weight' : 'shoot dry weight',
 'relative total dry weight' : 'plant dry weight',
 'relative water content' : 'leaf relative water content',
 'reproductive development' : 'reproductive homeotic development trait',
 'reproductive homeotic developme' : 'reproductive homeotic development trait',
 'root size' : 'root weight',
 'seed composition based quality trait' : 'seed quality trait',
 'seed dormancy' : 'seed growth and development trait',
 'seed viability' : 'seed quality trait',
 'shoot branching' : 'stem morphology trait',
 'shoot potassium content' : 'shoot system potassium content',
 'shoot system yield trait' : 'yield trait',
 'shoot weight' : 'shoot fresh weight',
 'shootÂ\xa0apicalÂ\xa0meristem development' : 'shoot apical meristem development',
 'stature or vigor trait' : 'plant vigor trait',
 'stem rot disease resistance' : 'fungal disease resistance',
 'stover organic matter digestability' : 'stover organic matter digestibility',
 'tiller angle' : 'shoot axis morphology trait',
 'tolerant to freezing' : 'cold tolerance',
 'total biomass yield' : 'yield trait',
 'vigor related trait' : 'plant vigor trait',
 'viral resistance trait' : 'viral disease resistance',
 'zinc content': 'zinc content trait'}


def merge_and_prepare_training_data():
    annotated = pd.read_csv(ANNOTATED_PATH)[['Entry', 'TO']].dropna()
    annotated['type'] = 'annotated'
    associated_rice = pd.read_parquet(rice_gene_data)[['Entry', 'TO']].dropna()
    associated_rice['type'] = 'association'
    associated_corn = pd.read_parquet(corn_gene_data)[['Entry', 'TO']].dropna()
    associated_corn['type'] = 'association'
    orthologs = pd.read_parquet(gene_data_of_orthologs)[['Entry', 'TO']].dropna()
    orthologs['type'] = 'orthologs'
    merged = pd.concat([annotated, associated_rice, associated_corn, orthologs], ignore_index=True)
    merged = merged.drop_duplicates(subset=['Entry', 'TO'])
    
    # fix problematic traits:
    merged['TO'] = merged['TO'].apply(lambda trait: trait_adjusments.get(trait, trait))
    merged['Entry'] = merged['Entry'].str.strip()
    return merged

def get_available_trait_ontologies():
    to_emb = pd.read_parquet(trait_ontology_bert_embeding_file)
    available_to = to_emb.index.values
    return available_to


def get_gene_data():
    return pd.read_parquet(NUMERIC_SCALED_MERGED_FILE_NAME)
    