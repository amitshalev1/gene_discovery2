#! pip install  sentencepiece  spacy ftfy==4.4.3 -q
# !pip install pytorch-pretrained-bert
# !python -m spacy download en

#! pip install biopython
#! wget https://s3-us-west-2.amazonaws.com/ai2-s2-research/scibert/pytorch_models/scibert_scivocab_uncased.tar
# ! tar -xvf scibert_scivocab_uncased.tar

from pytorch_pretrained_bert import BertTokenizer, BertModel
import torch
import numpy as np
import pandas as pd
from tqdm import tqdm 
# default bert weights:
# tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
# bert_model = BertModel.from_pretrained('bert-base-uncased')

#sci-bert weights:
bert_model = BertModel.from_pretrained('scibert_scivocab_uncased/weights.tar.gz')
tokenizer = BertTokenizer('scibert_scivocab_uncased/vocab.txt')

bert_model.eval()
# try:
#     bert_model.to('cuda')
# except:
#     pass


def pool_multi_layered_embeddings(emb):
    pooled = []
    for layer in emb:
      arr = layer.to('cpu').numpy().mean(axis=1)
      pooled.append(arr[0])
    pooled = np.array(pooled).mean(axis=0)
    return pooled

def sentence_to_tensor(sentence, tokenizer=tokenizer):
    tokenized_text = tokenizer.tokenize(sentence)[0:510]
    indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)
    segments_ids = [0 for i in enumerate(indexed_tokens)]

    # Convert inputs to PyTorch tensors
    tokens_tensor = torch.tensor([indexed_tokens])
    segments_tensors = torch.tensor([segments_ids])
#     try:
#         tokens_tensor = tokens_tensor.to('cuda')
#         segments_tensors = segments_tensors.to('cuda')
#     except AssertionError as e:
#         print(e)
        
    return tokens_tensor, segments_tensors

def apply_bert_model(tokens_tensor, segments_tensors, model=bert_model):
    # Predict hidden states features for each layer
    with torch.no_grad():
        encoded_layers, _ = model(tokens_tensor, segments_tensors)
    return encoded_layers   
       
def bert_embedding(sentences, model=bert_model, verbose=False):
    '''apply bert embedding on sentences usign defaults, and return the average-pooled embedding of each sentence'''
    embeddings = []
    if verbose: sentences = tqdm(sentences)
    for sentence in sentences:
        tokens_tensor, segments_tensors = sentence_to_tensor(sentence)
        encoded_layers = apply_bert_model(tokens_tensor, segments_tensors)
        pooled = pool_multi_layered_embeddings(encoded_layers)
        embeddings.append(pooled)
    
    return np.array(embeddings)



#to use ...
#bert_embedding(['plant bacterial resistence under drought', 'a second sentence right here'])
